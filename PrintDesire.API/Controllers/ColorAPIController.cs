﻿using PrintDesire.BusinessLayer.Implementation;
using PrintDesire.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrintDesire.API.Controllers
{
    public class ColorAPIController : ApiController
    {
        Colors objColors = null;

        public ColorAPIController()
        {
            objColors = new Colors();
        }

        public IEnumerable<ColorsModel> Get()
        {
            var result = objColors.GetColors();

            return result;
        }

        public ColorsModel Get(int colorId)
        {
            var result = objColors.GetColorById(colorId);

            return result;
        }

        public bool Post([FromBody]ColorsModel objColorsModel)
        {
            var result = objColors.AddColor(objColorsModel);

            return result;
        }

        public bool Put([FromBody]ColorsModel objColorsModel)
        {
            var result = objColors.UpdateColor(objColorsModel);

            return result;
        }

        public bool Delete(int colorId)
        {
            var productCategoryModel = objColors.GetColorById(colorId);

            var result = objColors.RemoveColor(productCategoryModel);

            return result;
        }
    }
}
