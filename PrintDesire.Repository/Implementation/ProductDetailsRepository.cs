﻿using PrintDesire.Repository.EntityDiagram;
using PrintDesire.Repository.Interface;
using System.Collections.Generic;

namespace PrintDesire.Repository.Implementation
{
   public class ProductDetailsRepository:IProductDetailsRepository
    {
       private readonly IProductDetailsWrapper _objProductDetailsWrapper;

       /// <summary>
       /// Initializes Mobile Company wrapper
       /// </summary>
        public ProductDetailsRepository()
        {
            _objProductDetailsWrapper = new ProductDetailsWrapper();
        }

       /// <summary>
        /// Initializes Mobile Company wrapper with parameter
       /// </summary>
       /// <param name="objMobileModelWrapper"></param>
        public ProductDetailsRepository(IProductDetailsWrapper objProductDetailsWrapper)
        {
            _objProductDetailsWrapper = objProductDetailsWrapper;
        }

       /// <summary>
        /// Gets all Mobile Model
       /// </summary>
        /// <returns>Mobile Model collection</returns>
        public IEnumerable<productdetails> GetProductDetails()
        {
            var result = _objProductDetailsWrapper.GetAll();

            return result;
        }

       /// <summary>
        /// Gets Mobile Model By ID
       /// </summary>
       /// <param name="mobileModelId"></param>
        /// <returns>Mobile Model</returns>
        public productdetails GetProductDetailsById(int productDetailsId)
        {
            var result = _objProductDetailsWrapper.GetSingle(x => x.ProductDetailsId == productDetailsId);

            return result;
        }

       /// <summary>
        /// Inserts Mobile Model
       /// </summary>
       /// <param name="objMobileModel"></param>
        /// <returns>Success result true/false</returns>
        public bool AddProductDetails(productdetails objProductDetails)
        {
            bool isSuccess = false;

            _objProductDetailsWrapper.Add(objProductDetails);

            isSuccess = true;

            return isSuccess;
        }

       /// <summary>
        /// Updates Mobile Model
       /// </summary>
       /// <param name="objMobileModel"></param>
        /// <returns>Success result true/false</returns>
        public bool UpdateProductDetails(productdetails objProductDetails)
        {
            bool isSuccess = false;

            _objProductDetailsWrapper.Update(objProductDetails);

            isSuccess = true;

            return isSuccess;
        }

       /// <summary>
        /// Deletes Mobile Model
       /// </summary>
       /// <param name="objMobileModel"></param>
        /// <returns>Success result true/false</returns>
        public bool RemoveProductDetails(productdetails objProductDetails)
        {
            bool isSuccess = false;

            _objProductDetailsWrapper.Remove(objProductDetails);

            isSuccess = true;

            return isSuccess;
        }

       /// <summary>
       /// Gets Mobile Model By Mobile Company ID
       /// </summary>
       /// <param name="mobileCompanyId"></param>
        /// <returns>Success result true/false</returns>
        public bool GetProductDetailsByProductCategoryId(int productCategoryId)
        {
            var mobileModel = _objProductDetailsWrapper.GetSingle(x => x.ProductCategoryId == productCategoryId);

            if (mobileModel != null)
            {
                return true;
            }

            return false;
        }

    }
}
