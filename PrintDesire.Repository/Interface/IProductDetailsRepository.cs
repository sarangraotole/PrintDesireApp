﻿using PrintDesire.Repository.EntityDiagram;
using System.Collections.Generic;

namespace PrintDesire.Repository.Interface
{
   public interface IProductDetailsRepository
    {
       IEnumerable<productdetails> GetProductDetails();

       productdetails GetProductDetailsById(int productDetailsId);

       bool AddProductDetails(productdetails objProductDetails);

       bool UpdateProductDetails(productdetails objProductDetails);

       bool RemoveProductDetails(productdetails objProductDetails);

       bool GetProductDetailsByProductCategoryId(int productCategoryId);
    }
}
