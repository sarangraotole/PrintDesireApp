﻿using PrintDesire.Repository.EntityDiagram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.Repository.Interface
{
   public interface IColorRepository
    {
       IEnumerable<colors> GetColors();
       colors GetColorById(int colorId);
       bool AddColor(colors objColors);
       bool UpdateColor(colors objColors);
       bool RemoveColor(colors objColors);
    }
}
