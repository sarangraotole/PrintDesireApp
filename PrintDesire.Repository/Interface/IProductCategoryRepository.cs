﻿using PrintDesire.Repository.EntityDiagram;
using System.Collections.Generic;

namespace PrintDesire.Repository.Interface
{
    public interface IProductCategoryRepository
    {
        IEnumerable<productcategory> GetProductCategory();

        productcategory GetProductCategoryById(int productCategoryId);

        bool AddProductCategory(productcategory objProductCategory);

        bool UpdateProductCategory(productcategory objProductCategory);

        bool RemoveProductCategory(productcategory objProductCategory);
    }
}
