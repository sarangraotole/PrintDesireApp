﻿using PrintDesire.Repository.EntityDiagram;
using PrintDesire.Repository.Repository;

namespace PrintDesire.Repository
{
    #region ProductCategory

    public interface IProductCategoryWrapper : IGenericRepository<productcategory>
    {
    }

    public class ProductCategoryWrapper : GenericRepository<productcategory>, IProductCategoryWrapper
    {
    }

    #endregion

    #region ProductDetails

    public interface IProductDetailsWrapper : IGenericRepository<productdetails>
    {
    }

    public class ProductDetailsWrapper : GenericRepository<productdetails>, IProductDetailsWrapper
    {
    }

    #endregion

    #region Colors

    public interface IColorWrapper : IGenericRepository<colors>
    {
    }

    public class ColorWrapper : GenericRepository<colors>, IColorWrapper
    {
    }

    #endregion

}
