﻿using System.Web.Mvc;

namespace PrintDesireApp.Controllers
{
    [Authorize]
    public class PrintWebController : Controller
    {        
        public ActionResult Index()
        {
         //   throw new Exception();
           return View();
        }

        public ActionResult Product_Listing()
        {
            return View();
        }
        public ActionResult Product_Details()
        {
            return View();
        }
        public ActionResult Product_Catalogue()
        {
            return View();
        }
        public ActionResult Shoping_Cart()
        {
            return View();
        }
        public ActionResult Checkout()
        {
            return View();
        }
        public ActionResult Compaire()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult MobileCover_Listing()
        {
            return View();
        }
        public ActionResult MobileCover_Details()
        {
            return View();
        }

        public ActionResult Payment_Details()
        {
            return View();
        }
        public ActionResult Vendor_Payment()
        {
            return View();
        }
        public ActionResult Text_Editor()
        {
            return View();
        }

    }
}