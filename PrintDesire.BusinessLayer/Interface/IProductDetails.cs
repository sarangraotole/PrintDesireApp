﻿using PrintDesire.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer.Interface
{
   public interface IProductDetails
    {

       IEnumerable<ProductDetailsModel> GetProductDetails();


       ProductDetailsModel GetProductDetailsById(int ProductDetailsId);


       bool AddProductDetails(ProductDetailsModel objProductDetails);


       bool UpdateProductDetails(ProductDetailsModel objProductDetails);


       bool RemoveProductDetails(ProductDetailsModel objProductDetails);


       bool GetProductDetailsByProductCategoryId(int productCategoryId);
    }
}
