﻿using PrintDesire.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer.Interface
{
   public interface IColors
    {
       IEnumerable<ColorsModel> GetColors();
       ColorsModel GetColorById(int colorId);
       bool AddColor(ColorsModel objColorsModel);
       bool UpdateColor(ColorsModel objColorsModel);
       bool RemoveColor(ColorsModel objColorsModel);
    }
}
