﻿using PrintDesire.BusinessLayer.Interface;
using PrintDesire.BusinessLayer.Models;
using PrintDesire.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer.Implementation
{
   public class ProductCategory:IProductCategory
    {
       private ProductCategoryRepository objProductCategoryRepository; 

        /// <summary>
        /// Constructor initializes Mobile Company repository
        /// </summary>
        public ProductCategory()
        {
            objProductCategoryRepository = new ProductCategoryRepository();
        }

        /// <summary>
        /// Gets all Mobile Company
        /// </summary>
        /// <returns>Mobile Company</returns>
        public IEnumerable<ProductCategoryModel> GetProductCategory()
        {            
            List<ProductCategoryModel> lstProductCategoryModel = new List<ProductCategoryModel>();

            var result = objProductCategoryRepository.GetProductCategory();
            return result.ToModelEntity().ToList();                       
        }

        /// <summary>
        /// Gets Mobile Company by ID
        /// </summary>
        /// <param name="mobileCompanyId"></param>
        /// <returns>Mobile Company</returns>
        public ProductCategoryModel GetProductCategoryById(int productCategoryId)
        {
            var result = objProductCategoryRepository.GetProductCategoryById(productCategoryId);

            return result.ToModelEntity();            
        }

        /// <summary>
        /// Inserts a new Mobile Company
        /// </summary>
        /// <param name="objMobileCompanyModel"></param>
        /// <returns>Success result true/false</returns>
        public bool AddProductCategory(ProductCategoryModel objProductCategoryModel)
        {            
            var productCategory = objProductCategoryModel.ToModelEntity();

            var result = objProductCategoryRepository.AddProductCategory(productCategory);                                   

            return result;
        }

        /// <summary>
        /// Updates a Mobile Company
        /// </summary>
        /// <param name="objMobileCompanyModel"></param>
        /// <returns>Success result true/false</returns>
        public bool UpdateProductCategory(ProductCategoryModel objProductCategoryModel)
        {
            var productCategory = objProductCategoryModel.ToModelEntity();

            var result = objProductCategoryRepository.UpdateProductCategory(productCategory);

            return result;
        }

        /// <summary>
        /// Deletes a Mobile Company
        /// </summary>
        /// <param name="objMobileCompanyModel"></param>
        /// <returns>Success result true/false</returns>
        public bool RemoveProductCategory(ProductCategoryModel objProductCategoryModel)
        {
            var productCategory = objProductCategoryModel.ToModelEntity();

            var result = objProductCategoryRepository.RemoveProductCategory(productCategory);

            return result;
        }
    }
}

