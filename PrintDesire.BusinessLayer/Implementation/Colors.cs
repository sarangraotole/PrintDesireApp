﻿using PrintDesire.BusinessLayer.Interface;
using PrintDesire.BusinessLayer.Models;
using PrintDesire.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer.Implementation
{
   public class Colors : IColors
    {
        private ColorRepository objColorRepository; 

        /// <summary>
        /// Constructor initializes Color repository
        /// </summary>
        public Colors()
        {
            objColorRepository = new ColorRepository();
        }

        /// <summary>
        /// Gets all Colors
        /// </summary>
        /// <returns>Colors collection</returns>
        public IEnumerable<ColorsModel> GetColors()
        {
            List<ColorsModel> lstColorsModel = new List<ColorsModel>();

            var result = objColorRepository.GetColors();
            return result.ToModelEntity().ToList();                       
        }

        /// <summary>
        /// Gets Color by ID
        /// </summary>
        /// <param name="colorId"></param>
        /// <returns>Color</returns>
        public ColorsModel GetColorById(int colorId)
        {
            var result = objColorRepository.GetColorById(colorId);

            return result.ToModelEntity();            
        }

        /// <summary>
        /// Inserts a new Color
        /// </summary>
        /// <param name="objColorModel"></param>
        /// <returns>Success result true/false</returns>
        public bool AddColor(ColorsModel objColorsModel)
        {
            var color = objColorsModel.ToModelEntity();

            var result = objColorRepository.AddColor(color);                                   

            return result;
        }

        /// <summary>
        /// Updates a Color
        /// </summary>
        /// <param name="objColorsModel"></param>
        /// <returns>Success result true/false</returns>
        public bool UpdateColor(ColorsModel objColorsModel)
        {
            var color = objColorsModel.ToModelEntity();

            var result = objColorRepository.UpdateColor(color);

            return result;
        }

        /// <summary>
        /// Deletes a Color
        /// </summary>
        /// <param name="objMobileCompanyModel"></param>
        /// <returns>Success result true/false</returns>
        public bool RemoveColor(ColorsModel objColorsModel)
        {
            var color = objColorsModel.ToModelEntity();

            var result = objColorRepository.RemoveColor(color);

            return result;
        }
    }
}
