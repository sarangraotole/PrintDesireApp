﻿using PrintDesire.BusinessLayer.Models;
using PrintDesire.Repository.EntityDiagram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer
{
    internal static class PropertyMapper
    {
        #region BindToModel

        #region ProductCategory

        internal static IEnumerable<ProductCategoryModel> ToModelEntity(this IEnumerable<productcategory> model)
        {
            List<ProductCategoryModel> lstProductCategoryModel = new List<ProductCategoryModel>();
            ProductCategoryModel objProductCategoryModel = null;

            foreach (var item in model)
            {
                objProductCategoryModel = new ProductCategoryModel();
                objProductCategoryModel.ProductCategoryId = item.ProductCategoryId;
                objProductCategoryModel.Name = item.Name;
                objProductCategoryModel.Description = item.Description;
                objProductCategoryModel.IsActive = item.IsActive;
                objProductCategoryModel.CreatedBy = item.CreatedBy;
                objProductCategoryModel.CreatedDate = item.CreatedDate;
                objProductCategoryModel.ModifiedBy = item.ModifiedBy;
                objProductCategoryModel.ModifiedDate = item.ModifiedDate;

                lstProductCategoryModel.Add(objProductCategoryModel);
            }

            return lstProductCategoryModel;
        }

        internal static ProductCategoryModel ToModelEntity(this productcategory model)
        {
            ProductCategoryModel objProductCategoryModel = new ProductCategoryModel();

            objProductCategoryModel.ProductCategoryId = model.ProductCategoryId;
            objProductCategoryModel.Name = model.Name;
            objProductCategoryModel.Description = model.Description;
            objProductCategoryModel.IsActive = model.IsActive;
            objProductCategoryModel.CreatedBy = model.CreatedBy;
            objProductCategoryModel.CreatedDate = model.CreatedDate;
            objProductCategoryModel.ModifiedBy = model.ModifiedBy;
            objProductCategoryModel.ModifiedDate = model.ModifiedDate;

            return objProductCategoryModel;
        }

        #endregion

        #region ProductDetails

        internal static IEnumerable<ProductDetailsModel> ToModelEntity(this IEnumerable<productdetails> model)
        {
            List<ProductDetailsModel> lstProductDetailsModel = new List<ProductDetailsModel>();
            ProductDetailsModel objProductDetailsModel = null;

            foreach (var item in model)
            {
                objProductDetailsModel = new ProductDetailsModel();
                objProductDetailsModel.ProductDetailsId = item.ProductDetailsId;
                objProductDetailsModel.ProductCategoryId = item.ProductCategoryId;
                objProductDetailsModel.Description = item.Description;
                objProductDetailsModel.ImagePath = item.ImagePath;
                objProductDetailsModel.IdealFor = item.IdealFor;
                objProductDetailsModel.Finishing = item.Finishing;
                objProductDetailsModel.Color = item.Color;
                objProductDetailsModel.IsActive = item.IsActive;
                objProductDetailsModel.WaterProof = item.WaterProof;
                objProductDetailsModel.Material = item.Material;
                objProductDetailsModel.CreatedBy = item.CreatedBy;
                objProductDetailsModel.ModifiedBy = item.ModifiedBy;
                objProductDetailsModel.CreatedDate = item.CreatedDate;
                objProductDetailsModel.ModifiedDate = item.ModifiedDate;

                lstProductDetailsModel.Add(objProductDetailsModel);
            }

            return lstProductDetailsModel;
        }

        internal static ProductDetailsModel ToModelEntity(this productdetails model)
        {
            ProductDetailsModel objProductDetailsModel = new ProductDetailsModel();

            objProductDetailsModel.ProductDetailsId = model.ProductDetailsId;
            objProductDetailsModel.ProductCategoryId = model.ProductCategoryId;
            objProductDetailsModel.Description = model.Description;
            objProductDetailsModel.ImagePath = model.ImagePath;
            objProductDetailsModel.IdealFor = model.IdealFor;
            objProductDetailsModel.Finishing = model.Finishing;
            objProductDetailsModel.Color = model.Color;
            objProductDetailsModel.IsActive = model.IsActive;
            objProductDetailsModel.WaterProof = model.WaterProof;
            objProductDetailsModel.Material = model.Material;
            objProductDetailsModel.CreatedBy = model.CreatedBy;
            objProductDetailsModel.ModifiedBy = model.ModifiedBy;
            objProductDetailsModel.CreatedDate = model.CreatedDate;
            objProductDetailsModel.ModifiedDate = model.ModifiedDate;

            return objProductDetailsModel;
        }

        #endregion

        #region Colors
        internal static IEnumerable<ColorsModel> ToModelEntity(this IEnumerable<colors> model)
        {
            List<ColorsModel> lstColorsModel = new List<ColorsModel>();
            ColorsModel objColorsModel = null;

            foreach (var item in model)
            {
                objColorsModel = new ColorsModel();
                objColorsModel.ColorId = item.ColorId;
                objColorsModel.Name = item.Name;
                objColorsModel.Description = item.Description;
                objColorsModel.CreatedBy = item.CreatedBy;
                objColorsModel.CreatedDate = item.CreatedDate;
                objColorsModel.ModifiedBy = item.ModifiedBy;
                objColorsModel.ModifiedDate = item.ModifiedDate;

                lstColorsModel.Add(objColorsModel);
            }

            return lstColorsModel;
        }

        internal static ColorsModel ToModelEntity(this colors model)
        {
            ColorsModel objColorsModel = new ColorsModel();

            objColorsModel.ColorId = model.ColorId;
            objColorsModel.Name = model.Name;
            objColorsModel.Description = model.Description;            
            objColorsModel.CreatedBy = model.CreatedBy;
            objColorsModel.CreatedDate = model.CreatedDate;
            objColorsModel.ModifiedBy = model.ModifiedBy;
            objColorsModel.ModifiedDate = model.ModifiedDate;

            return objColorsModel;
        }
        #endregion

        #endregion

        #region BindToDataEntity

        #region ProductCategory

        internal static productcategory ToModelEntity(this ProductCategoryModel model)
        {
            productcategory objProductCategory = new productcategory();

            objProductCategory.ProductCategoryId = model.ProductCategoryId;
            objProductCategory.Name = model.Name;
            objProductCategory.Description = model.Description;
            objProductCategory.IsActive = model.IsActive;
            objProductCategory.CreatedBy = model.CreatedBy;
            objProductCategory.CreatedDate = model.CreatedDate;
            objProductCategory.ModifiedBy = model.ModifiedBy;
            objProductCategory.ModifiedDate = model.ModifiedDate;

            return objProductCategory;
        }

        #endregion

        #region ProductDetails

        internal static productdetails ToModelEntity(this ProductDetailsModel model)
        {
            productdetails objproductdetails = new productdetails();

            objproductdetails.ProductDetailsId = model.ProductDetailsId;
            objproductdetails.ProductCategoryId = model.ProductCategoryId;
            objproductdetails.Description = model.Description;
            objproductdetails.ImagePath = model.ImagePath;
            objproductdetails.IdealFor = model.IdealFor;
            objproductdetails.Finishing = model.Finishing;
            objproductdetails.Color = model.Color;
            objproductdetails.IsActive = model.IsActive;
            objproductdetails.WaterProof = model.WaterProof;
            objproductdetails.Material = model.Material;
            objproductdetails.CreatedBy = model.CreatedBy;
            objproductdetails.CreatedDate = model.CreatedDate;
            objproductdetails.ModifiedBy = model.ModifiedBy;
            objproductdetails.ModifiedDate = model.ModifiedDate;

            return objproductdetails;
        }

        #endregion

        #region Colors

        internal static colors ToModelEntity(this ColorsModel model)
        {
            colors objColors = new colors();

            objColors.ColorId = model.ColorId;
            objColors.Name = model.Name;
            objColors.Description = model.Description;            
            objColors.CreatedBy = model.CreatedBy;
            objColors.CreatedDate = model.CreatedDate;
            objColors.ModifiedBy = model.ModifiedBy;
            objColors.ModifiedDate = model.ModifiedDate;

            return objColors;
        }

        #endregion

        #endregion
    }
}
